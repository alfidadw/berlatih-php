
<?php
function tukar_besar_kecil($string){
    $output = "";
    for ($i=0; $i<strlen($string); $i++){
        if (ctype_upper($string[$i])){
            $output .= strtolower($string[$i]);
        }else{
            $output .= strtoupper($string[$i]);
        }
    }
        return $output . "<br>";
} 

// TEST CASES
echo tukar_besar_kecil('Hello World');
echo tukar_besar_kecil('I aM aLAY'); 
echo tukar_besar_kecil('My Name is Bond!!');
echo tukar_besar_kecil('IT sHOULD bE me');
echo tukar_besar_kecil('001-A-3-5TrdYW');

?>